package com.apitechu.mongoDB.services;

import com.apitechu.mongoDB.models.ProductModel;
import com.apitechu.mongoDB.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findALl() {
        System.out.println("findAll en ProductService");
        return this.productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("find by id en product service");

        return this.productRepository.findById(id);
    }

    public ProductModel add(ProductModel product) {
        System.out.println("add en ProductServices");

        return this.productRepository.save(product);
    }

    public ProductModel update(ProductModel product) {
        System.out.println("update en productModel");
        return this.productRepository.save(product);
    }

    public boolean delete(String id) {
        boolean result = false;
        System.out.println("delete en productService");
        if (this.findById(id).isPresent() == true) {
            System.out.println("Producto encontrado, borrando");
            this.productRepository.deleteById(id);
            result = true;
        } else
            System.out.println("Producto no encontrado");
        return result;
    }
}
