package com.apitechu.mongoDB.services;

import com.apitechu.mongoDB.models.UserModel;
import com.apitechu.mongoDB.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;
    public List findAll(String orderBy) {

        System.out.println("findAll en UserService");
        List<UserModel> res;
        if (orderBy!=null){
            System.out.println("Se ha pedido ordenación");
            res=this.userRepository.findAll(Sort.by("age").descending());
        }
        else {
            res=this.userRepository.findAll();
        }
        return res;
    }

    public Optional<UserModel> findById(String id) {
        System.out.println("find by id en userservice");
        return this.userRepository.findById(id);
    }

    public UserModel addUser(UserModel user) {
        System.out.println("addUser en ProductServices");

        return this.userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("update en userService");

        return this.userRepository.save(user);
    }

    public boolean delete(String id) {
        boolean result = false;
        System.out.println("delete en UserService");
        if (this.findById(id).isPresent() == true) {
            System.out.println("User encontrado, borrando");
            this.userRepository.deleteById(id);
            result = true;
        } else
            System.out.println("User no encontrado");
        return result;
    }
}
