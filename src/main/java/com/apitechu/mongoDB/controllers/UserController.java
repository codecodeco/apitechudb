package com.apitechu.mongoDB.controllers;

import com.apitechu.mongoDB.models.UserModel;
import com.apitechu.mongoDB.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name = "$orderby", required = false) String orderBy) {
        System.out.println("getUsers controller");
        return new ResponseEntity<>(
                this.userService.findAll(orderBy),
                HttpStatus.OK
        );
    }

    @GetMapping("/users/{id}")
    public ResponseEntity<?> getUsersById(@PathVariable String id) {
        System.out.println("getUsersByID controller");
        System.out.println("La id del user a buscar es:" + id);

        Optional<UserModel> res = this.userService.findById(id);
        return new ResponseEntity<>(
                res.isPresent() ? res.get() : "User no encontrado",
                res.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) {
        System.out.println("addUser en UserController");
        System.out.println("La id del user a crear es :" + userModel.getId());
        System.out.println("La age del user a crear es :" + userModel.getAge());
        System.out.println("La name del producto a crear es :" + userModel.getName());

        return new ResponseEntity<>(
                this.userService.addUser(userModel),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("updateUser controller");
        System.out.println("La id del user modificado:" + id);
        System.out.println("La id del user a actualizar es:" + user.getId());
        System.out.println("La name del user a actualizar es:" + user.getName());
        System.out.println("La age del user a actualizar es:" + user.getAge());

        Optional<UserModel> userUpdate = this.userService.findById(id);
        if (userUpdate.isPresent()) {
            System.out.println("User encotnrado, comenzamos a actualizar");
            this.userService.update(user);
        } else {
            System.out.println("User no encontrado, no actualizar");
        }
        return new ResponseEntity<>(
                user, userUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable String id) {
        System.out.println("deleteUser");
        System.out.println("La id del user a borrar es:" + id);

        boolean deleteUser = this.userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "User borrado" : "User no encontrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
