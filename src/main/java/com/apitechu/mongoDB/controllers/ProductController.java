package com.apitechu.mongoDB.controllers;

import com.apitechu.mongoDB.models.ProductModel;
import com.apitechu.mongoDB.repositories.ProductRepository;
import com.apitechu.mongoDB.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductController {
    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("getProducts");
        return new ResponseEntity<>(this.productService.findALl(),
                HttpStatus.OK);
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<?> getProductById(@PathVariable String id) {
        System.out.println("getProductByID");
        System.out.println("La id del producto a buscar es:" + id);

        Optional<ProductModel> res = this.productService.findById(id);
        return new ResponseEntity <>(
                res.isPresent()?res.get():"Producto no encontrado",
                res.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
                );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel productModel) {
        System.out.println("addProduct");
        System.out.println("La id del producto a crear es :" + productModel.getId());
        System.out.println("La desc del producto a crear es :" + productModel.getDesc());
        System.out.println("La price del producto a crear es :" + productModel.getPrice());

        return new ResponseEntity<>(
                this.productService.add(productModel),
                HttpStatus.CREATED
        );
    }
    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product,@PathVariable String id){
        System.out.println("updatePRoduct");
        System.out.println("La id del producto a en url es:" + id);
        System.out.println("La id del producto a actualizar es:" + product.getId());
        System.out.println("La des del producto a actualizar es:" + product.getDesc());
        System.out.println("La price del producto a actualizar es:" + product.getPrice());

        Optional<ProductModel> productUpdate = this.productService.findById(id);
        if(productUpdate.isPresent()){
            System.out.println("Producto encontrado, actualizando");
            this.productService.update(product);
        }else
            System.out.println("Producto no encontrado");

        return new ResponseEntity<>(
            product,productUpdate.isPresent()?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }
    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");
        System.out.println("La id del producto a borrar es:" + id);

        boolean deleteProduct = this.productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct?"Producto borrado":"Producto no encontrado",
                deleteProduct?HttpStatus.OK:HttpStatus.NOT_FOUND
        );
    }
}
