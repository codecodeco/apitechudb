package com.apitechu.mongoDB;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MongoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoDbApplication.class, args);
	}

}

//Consulta a la DB
//Modelo
//Repositorio
//Consulta
//Conexion
//Service
//Controlador